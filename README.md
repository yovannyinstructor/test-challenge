# LEAL-TEST-CHALLENGE

Proyecto creado en el framework Nest.js, como prueba técnica para el cargo de Backend Developer en Leal.co

---

## REQUERIMIENTOS PREVIOS ⚙️

<div>
<img src="https://img.shields.io/static/v1.svg?label=Node.js&message=%20%3E=%20v18.16.1&labelColor=339933&color=757575&logoColor=FFFFFF&logo=node.js" alt="Node.js" width="25%" />
<img src="https://img.shields.io/static/v1.svg?label=Nest.js&message=%20%3E=%20v9.0.0&labelColor=ea2745&logoColor=FFFFFF&color=757575&logo=nestjs" alt="Nest.js" width="25%" />
<img src="https://img.shields.io/static/v1.svg?label=Typescript&message=%20%3E=%20v4.9.5&labelColor=3179c6&logoColor=FFFFFF&color=757575&logo=typescript" alt="Typescript" width="25%" />
<img src="https://img.shields.io/static/v1.svg?label=Docker&message=%20%3E=%20v24.0.0&labelColor=3179c6&logoColor=FFFFFF&color=757575&logo=Docker" alt="Docker" width="25%" />
</div>

---

## CONFIGURACIÓN INICIAL 🏗️

Recomiendo usar node version manager (nvm) y npm run

1. Definir versión de node

```sh
$ nvm alias default v18.16.1
```

2. Crear un archivo .env, con las mismas variables de entorno del archivo .env.example

3. Instalar dependencias

```sh
$ npm install
```

4. Crear una base de datos local con el nombre de "leal-test-challenge", usando el motor postgresql o crear una imagen de docker 🐳 ejecutando el comando:

```sh
$ npm run docker
```

o

```sh
$ docker-compose up
```

5. Ejecutar migraciones:

```sh
$ npm run migrate:run
```

6. Ejecutar semillas (opcional):

```sh
$ npm run seed:run
```

---

## SCRIPTS 🎨

- 🧑‍💻 Correr aplicación en ambiente de desarrollo

```bash
$ npm run start:dev
```

- 🚀 Correr aplicación en ambiente de producción

```bash
$ npm run start:prod
```

- 🧪 Ejecutar pruebas unitarias

```bash
$ npm run test
```

- 🧪 Ejecutar pruebas unitarias y cobertura de pruebas

```bash
$ npm run test:cov
```

---

## Linter ✅

Uso ESlint y Prettier.

```bash
# ESLint
$ npm run lint

# Code format
$ npm run format

# Prettier lint
$ npm run prettier
```

---

## DOCUMENTACIÓN 📄

### Swagger

Solamente disponible en ambiente de desarrollo.

Puede accederse desde la ruta {{url}}/swagger.

Ejemplo:

```bash
http://localhost:3000/swagger
```

---

## ESTÁNDARES DE CODIFICACIÓN 🚩

Uso husky y commitlint para pre-commit y convención de commits

Más información:

- [Semantic versioning](https://semver.org/)
- [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0-beta.4/)

---

## ESTADO DEL MICROSERVICIO 🟢

Se puede verificar el estado o salud del microservicio:

Ejemplo:

```bash
http://localhost:3000/health
```

---

## AUTOR 👷

- Yovanny López (Backend Developer)

---

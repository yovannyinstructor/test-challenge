import { Logger } from '@nestjs/common';
import { Knex } from 'knex';

const TABLE_NAME = 'campaigns';

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex(TABLE_NAME).del();

  // Inserts seed entries
  Logger.log(`insert seeds on ${TABLE_NAME} table`);
  await knex(TABLE_NAME).insert([
    {
      id: 1,
      name: 'campaña 1',
      description: 'acumula 1 punto por cada $1000 de compra',
      min_quantity: 1000,
      reward_factor: 1,
      is_active: true,
      start_date: '2023-01-01',
      end_date: '2023-12-31',
      branch_id: 1,
      merchant_id: 1,
    },
    {
      id: 2,
      name: 'campaña 1',
      description: 'acumula 1 punto por cada $1000 de compra',
      min_quantity: 1000,
      reward_factor: 1,
      is_active: true,
      start_date: '2023-01-01',
      end_date: '2023-12-31',
      branch_id: 2,
      merchant_id: 1,
    },
    {
      id: 3,
      name: 'campaña 2',
      description:
        'acumula el doble de puntos por cada compra en la fecha establecida',
      min_quantity: 1000,
      reward_factor: 2,
      is_active: true,
      start_date: '2023-07-04',
      end_date: '2023-07-15',
      branch_id: 1,
      merchant_id: 1,
    },
    {
      id: 4,
      name: 'campaña 3',
      description: 'obtienes 30% adicional de puntos',
      min_quantity: 20000,
      reward_factor: 1.3,
      is_active: true,
      start_date: '2023-07-04',
      end_date: '2023-07-15',
      branch_id: 2,
      merchant_id: 1,
    },
  ]);
}

import { Logger } from '@nestjs/common';
import { Knex } from 'knex';

const TABLE_NAME = 'customers';

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex(TABLE_NAME).del();

  // Inserts seed entries
  Logger.log(`insert seeds on ${TABLE_NAME} table`);
  await knex(TABLE_NAME).insert([
    {
      id: 1,
      name: 'yovanny lopez',
      email: 'yovanny@gmail.com',
      merchant_id: 1,
    },
    {
      id: 2,
      name: 'nicolas lopez',
      email: 'nicolas@gmail.com',
      merchant_id: 1,
    },
  ]);
}

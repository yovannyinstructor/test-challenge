import { Logger } from '@nestjs/common';
import { Knex } from 'knex';

const TABLE_NAME = 'branches';

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex(TABLE_NAME).del();

  // Inserts seed entries
  Logger.log(`insert seeds on ${TABLE_NAME} table`);
  await knex(TABLE_NAME).insert([
    { id: 1, name: 'sucursal 1', merchant_id: 1 },
    { id: 2, name: 'sucursal 2', merchant_id: 1 },
    { id: 3, name: 'sucursal 1', merchant_id: 2 },
    { id: 4, name: 'sucursal 1', merchant_id: 3 },
  ]);
}

import { Logger } from '@nestjs/common';
import { Knex } from 'knex';

const TABLE_NAME = 'merchants';

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex(TABLE_NAME).del();

  // Inserts seed entries
  Logger.log(`insert seeds on ${TABLE_NAME} table`);
  await knex(TABLE_NAME).insert([
    { id: 1, name: 'texaco', nit: '1111-1' },
    { id: 2, name: 'comercio 2', nit: '2222-2' },
    { id: 3, name: 'comercio 3', nit: '3333-3' },
  ]);
}

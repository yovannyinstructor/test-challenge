import { Knex } from 'knex';
import { Logger } from '@nestjs/common';

const TABLE_NAME = 'rewards';

export async function up(knex: Knex): Promise<any> {
  if (await knex.schema.hasTable(TABLE_NAME)) {
    return;
  }

  Logger.log(`Creating ${TABLE_NAME} table`);

  return knex.schema.createTable(TABLE_NAME, (table: Knex.TableBuilder) => {
    table.increments('id').unsigned().primary();
    table.integer('points');
    table
      .integer('customer_id')
      .unsigned()
      .index()
      .references('id')
      .inTable('customers')
      .onDelete('CASCADE');
    table.timestamps(true, true);
  });
}

export async function down(knex: Knex): Promise<any> {
  Logger.log(`Droping ${TABLE_NAME} table`);
  return knex.schema.dropTableIfExists(TABLE_NAME);
}

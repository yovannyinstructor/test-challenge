import { Knex } from 'knex';
import { Logger } from '@nestjs/common';

const TABLE_NAME = 'campaigns';

export async function up(knex: Knex): Promise<any> {
  if (await knex.schema.hasTable(TABLE_NAME)) {
    return;
  }

  Logger.log(`Creating ${TABLE_NAME} table`);

  return knex.schema.createTable(TABLE_NAME, (table: Knex.TableBuilder) => {
    table.increments('id').unsigned().primary();
    table.string('name');
    table.string('description');
    table.integer('min_quantity');
    table.float('reward_factor');
    table.date('start_date').notNullable();
    table.date('end_date').notNullable();
    table.boolean('is_active');
    table
      .integer('branch_id')
      .unsigned()
      .index()
      .references('id')
      .inTable('branches')
      .onDelete('CASCADE');
    table
      .integer('merchant_id')
      .unsigned()
      .index()
      .references('id')
      .inTable('merchants')
      .onDelete('CASCADE');
    table
      .timestamp('created_at')
      .defaultTo(knex.fn.now())
      .notNullable()
      .index();
  });
}

export async function down(knex: Knex): Promise<any> {
  Logger.log(`Droping ${TABLE_NAME} table`);
  return knex.schema.dropTableIfExists(TABLE_NAME);
}

import { CreateBranchDto } from 'branches/domain/dtos/create-branch.dto';
import { BranchModel } from 'branches/domain/models/branch.model';

export const mockBranches = {
  items: [
    {
      id: 1,
      name: 'Branch 1',
      merchant_id: 1,
    },
  ],
  count: 1,
};

export const mockBranch: BranchModel = {
  id: 1,
  name: 'Branch 1',
  merchant_id: 1,
};

export const mockCreateBranchDto: CreateBranchDto = {
  name: 'New Branch',
  merchant_id: 1,
};

export const mockBranchesExpectedResponse = {
  items: [
    { id: 1, name: 'Branch 1', merchant_id: 1 },
    { id: 2, name: 'Branch 2', merchant_id: 1 },
  ],
  count: 2,
};

export const mockBranchExpectedResponse: BranchModel = {
  id: 1,
  name: 'Branch 1',
  merchant_id: 1,
};

import { MerchantModel } from 'merchants/domain/models/merchant.model';
import { mockTimestamps } from '../../timestamps.mock';
import { CreateMerchantDto } from 'merchants/domain/dtos/create-merchant.dto';

export const mockMerchants = {
  items: [
    {
      name: 'Merchant 1',
      nit: '1234567890',
    },
  ],
  count: 1,
};

export const mockMerchant: MerchantModel = {
  id: 1,
  name: 'Merchant 1',
  nit: '1234567890',
};

export const mockCreateMerchantDto: CreateMerchantDto = {
  name: 'Merchant 1',
  nit: '1234567890',
};

export const mockMerchantsExpectedResponse = {
  items: [
    {
      id: 1,
      name: 'Merchant 1',
      nit: '1234567890',
    },
    {
      id: 2,
      name: 'Merchant 1',
      nit: '1234567890',
    },
  ],
  count: 2,
};

export const mockMerchantExpectedResponse: MerchantModel = {
  id: 1,
  name: 'Merchant 1',
  nit: '1234567890',
};

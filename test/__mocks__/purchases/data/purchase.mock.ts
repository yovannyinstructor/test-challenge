import { CreatePurchaseDto } from 'purchases/domain/dtos/create-purchase.dto';
import { PurchaseModel } from 'purchases/domain/models/purchase.model';

export const mockPurchases = {
  items: [
    {
      id: 1,
      amount: 30000,
      branch_id: 1,
      campaign_id: 1,
      customer_id: 1,
    },
  ],
  count: 1,
};

export const mockPurchase: PurchaseModel = {
  id: 1,
  amount: 30000,
  branch_id: 1,
  campaign_id: 1,
  customer_id: 1,
};

export const mockCreatePurchaseDto: CreatePurchaseDto = {
  amount: 30000,
  branch_id: 1,
  campaign_id: 1,
  customer_id: 1,
};

export const mockPurchasesExpectedResponse = {
  items: [
    {
      id: 1,
      amount: 30000,
      branch_id: 1,
      campaign_id: 1,
      customer_id: 1,
    },
    {
      id: 2,
      amount: 30000,
      branch_id: 1,
      campaign_id: 1,
      customer_id: 1,
    },
  ],
  count: 2,
};

export const mockPurchaseExpectedResponse: PurchaseModel = {
  id: 1,
  amount: 30000,
  branch_id: 1,
  campaign_id: 1,
  customer_id: 1,
};

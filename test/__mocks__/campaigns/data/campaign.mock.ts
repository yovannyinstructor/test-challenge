import { CampaignModel } from 'campaigns/domain/models/campaign.model';
import { mockTimestamps } from '../../timestamps.mock';
import { CreateCampaignDto } from 'campaigns/domain/dtos/create-campaign.dto';

export const mockCampaigns = {
  items: [
    {
      id: 1,
      name: 'campaign 1',
      description: '',
      min_quantity: 1000,
      reward_factor: 1,
      start_date: '',
      end_date: '',
      is_active: true,
      branch_id: 1,
      merchant_id: 1,
    },
  ],
  count: 1,
};

export const mockCampaign: CampaignModel = {
  id: 1,
  name: 'campaign 1',
  description: '',
  min_quantity: 1000,
  reward_factor: 1,
  start_date: '',
  end_date: '',
  is_active: true,
  branch_id: 1,
  merchant_id: 1,
  ...mockTimestamps,
};

export const mockCreateCampaignDto: CreateCampaignDto = {
  name: 'campaign 1',
  description: '',
  min_quantity: 1000,
  reward_factor: 1,
  start_date: '',
  end_date: '',
  is_active: true,
  branch_id: 1,
  merchant_id: 1,
};

export const mockCampaignsExpectedResponse = {
  items: [
    {
      id: 1,
      name: 'campaign 1',
      description: '',
      min_quantity: 1000,
      reward_factor: 1,
      start_date: '',
      end_date: '',
      is_active: true,
      branch_id: 1,
      merchant_id: 1,
    },
    {
      id: 2,
      name: 'campaign 1',
      description: '',
      min_quantity: 1000,
      reward_factor: 1,
      start_date: '',
      end_date: '',
      is_active: true,
      branch_id: 1,
      merchant_id: 1,
    },
  ],
  count: 2,
};

export const mockCampaignExpectedResponse: CampaignModel = {
  id: 1,
  name: 'campaign 1',
  description: '',
  min_quantity: 1000,
  reward_factor: 1,
  start_date: '',
  end_date: '',
  is_active: true,
  branch_id: 1,
  merchant_id: 1,
};

import { CustomerModel } from 'customers/domain/models/customer.model';
import { mockTimestamps } from '../../timestamps.mock';
import { CreateCustomerDto } from 'customers/domain/dtos/create-customer.dto';

export const mockCustomers = {
  items: [
    {
      id: 1,
      name: 'Customer 1',
      email: 'customer1@gmail.com',
      merchant_id: 1,
    },
  ],
  count: 1,
};

export const mockCustomer: CustomerModel = {
  id: 1,
  name: 'Customer 1',
  email: 'customer1@gmail.com',
  merchant_id: 1,
  ...mockTimestamps,
};

export const mockCreateCustomerDto: CreateCustomerDto = {
  name: 'Customer 1',
  email: 'customer1@gmail.com',
  merchant_id: 1,
};

export const mockCustomersExpectedResponse = {
  items: [
    {
      id: 1,
      name: 'Customer 1',
      email: 'customer1@gmail.com',
      merchant_id: 1,
    },
    {
      id: 2,
      name: 'Customer 1',
      email: 'customer1@gmail.com',
      merchant_id: 1,
    },
  ],
  count: 2,
};

export const mockCustomerExpectedResponse: CustomerModel = {
  id: 1,
  name: 'Customer 1',
  email: 'customer1@gmail.com',
  merchant_id: 1,
};

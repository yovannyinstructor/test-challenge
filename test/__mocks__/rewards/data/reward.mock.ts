import { AssignRewardDto } from 'rewards/domain/dtos/assign-reward.dto';
import RewardModel from 'rewards/domain/models/reward.model';

export const mockRewards = {
  items: [
    {
      id: 1,
      points: 10000,
      customer_id: 1,
    },
  ],
  count: 1,
};

export const mockReward: RewardModel = {
  id: 1,
  points: 10000,
  customer_id: 1,
};

export const mockAssignRewardDto: AssignRewardDto = {
  points: 10000,
  customer_id: 1,
};

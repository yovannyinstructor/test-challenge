import { Module } from '@nestjs/common';

import { CampaignsService } from './application/services/campaigns.service';
import { CampaignsController } from './infrastructure/controllers/campaigns.controller';
import { CampaignsRepository } from './infrastructure/repositories/campaigns.repository';

@Module({
  imports: [],
  controllers: [CampaignsController],
  providers: [CampaignsService, CampaignsRepository],
})
export class CampaignsModule {}

import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateCampaignDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: 'name',
    example: 'promo del mes',
  })
  readonly name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: 'description',
    example: 'acumula el doble de puntos por cada compra',
  })
  readonly description: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    name: 'min_quantity',
    example: 20000,
  })
  readonly min_quantity: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    name: 'reward_factor',
    example: 2,
  })
  readonly reward_factor: number;

  @IsBoolean()
  @IsNotEmpty()
  @ApiProperty({
    name: 'is_active',
    example: true,
  })
  readonly is_active: boolean;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: '2023-07-01',
    description: 'Fecha de inicio de la campaña',
  })
  readonly start_date: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: '2023-07-31',
    description: 'Fecha de fin de la campaña',
  })
  readonly end_date: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    name: 'branch_id',
    example: 1,
  })
  readonly branch_id: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    name: 'merchant_id',
    example: 1,
  })
  readonly merchant_id: number;
}

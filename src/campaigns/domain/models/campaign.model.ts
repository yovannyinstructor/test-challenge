export interface CampaignModelData {
  id: number;
  name: string;
  description: string;
  min_quantity: number;
  reward_factor: number;
  is_active: boolean;
  start_date: string;
  end_date: string;
  branch_id: number;
  merchant_id: number;
}

export class CampaignModel {
  id: number;
  name: string;
  description: string;
  min_quantity: number;
  reward_factor: number;
  is_active: boolean;
  start_date: string;
  end_date: string;
  branch_id: number;
  merchant_id: number;

  constructor(campaignData: CampaignModelData) {
    this.id = campaignData.id;
    this.name = campaignData.name;
    this.description = campaignData.description;
    this.min_quantity = campaignData.min_quantity;
    this.reward_factor = campaignData.reward_factor;
    this.is_active = campaignData.is_active;
    this.start_date = campaignData.start_date;
    this.end_date = campaignData.end_date;
    this.branch_id = campaignData.branch_id;
    this.merchant_id = campaignData.merchant_id;
  }
}

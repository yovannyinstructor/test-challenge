import { Injectable } from '@nestjs/common';

import { CreateCampaignDto } from '../../domain/dtos/create-campaign.dto';
import { CampaignsRepository } from '../../infrastructure/repositories/campaigns.repository';

@Injectable()
export class CampaignsService {
  constructor(private readonly campaignsRepository: CampaignsRepository) {}

  getCampaigns(offset?: number, limit?: number, idsToSkip?: number) {
    return this.campaignsRepository.get(offset, limit, idsToSkip);
  }

  getCampaignById(id: number) {
    return this.campaignsRepository.getById(id);
  }

  createCampaign(createCampaignDto: CreateCampaignDto) {
    return this.campaignsRepository.create(createCampaignDto);
  }
}

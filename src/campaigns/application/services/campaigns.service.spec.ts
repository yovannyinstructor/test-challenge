import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { CampaignsService } from './campaigns.service';
import { CampaignsRepository } from 'campaigns/infrastructure/repositories/campaigns.repository';

import {
  mockCampaignExpectedResponse,
  mockCampaignsExpectedResponse,
  mockCreateCampaignDto,
} from '../../../../test/__mocks__/campaigns/data/campaign.mock';
import DatabaseService from '@shared/database/database.service';

describe('CampaignsService test suite 🧪', () => {
  let service: CampaignsService;
  let repository: CampaignsRepository;
  let databaseService: DatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CampaignsService,
        {
          provide: CampaignsRepository,
          useValue: {
            get: jest.fn(),
            getById: jest.fn(),
            create: jest.fn(),
          },
        },
        {
          provide: DatabaseService,
          useValue: {
            runQuery: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<CampaignsService>(CampaignsService);
    repository = module.get<CampaignsRepository>(CampaignsRepository);
  });

  it('should return campaigns with count ✅', async () => {
    jest
      .spyOn(repository, 'get')
      .mockResolvedValue(mockCampaignsExpectedResponse);

    const result = await service.getCampaigns();

    expect(result).toEqual(mockCampaignsExpectedResponse);
  });

  it('should return a branch by id ✅', async () => {
    jest
      .spyOn(repository, 'getById')
      .mockResolvedValue(mockCampaignExpectedResponse);

    const result = await service.getCampaignById(1);

    expect(result).toEqual(mockCampaignExpectedResponse);
  });

  it('should throw NotFoundException when branch is not found 🛑', async () => {
    jest
      .spyOn(repository, 'getById')
      .mockRejectedValue(new NotFoundException());

    await expect(service.getCampaignById(1)).rejects.toThrowError(
      NotFoundException,
    );
  });

  it('should create a new branch ✅', async () => {
    jest
      .spyOn(repository, 'create')
      .mockResolvedValue(mockCampaignExpectedResponse);

    const result = await service.createCampaign(mockCreateCampaignDto);

    expect(result).toEqual(mockCampaignExpectedResponse);
  });

  it('should throw BadRequestException for invalid createCampaignDto 🛑', async () => {
    jest
      .spyOn(repository, 'create')
      .mockRejectedValue(new BadRequestException());

    await expect(
      service.createCampaign(mockCreateCampaignDto),
    ).rejects.toThrowError(BadRequestException);
  });
});

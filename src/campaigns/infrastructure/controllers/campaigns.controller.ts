import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiOperation,
  ApiProduces,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import { ErrorBadRequestDto } from '@shared/dtos/error.dto';
import { CAMPAIGNS_PATH, CAMPAIGNS_TAG } from '@shared/swagger/constants';
import FindOneParams from '@shared/utils/findOneParams';
import PaginationParams from '@shared/utils/paginationParams';

import { CampaignsService } from '../../application/services/campaigns.service';
import { CreateCampaignDto } from '../../domain/dtos/create-campaign.dto';

@ApiTags(CAMPAIGNS_TAG)
@Controller(CAMPAIGNS_PATH)
@UseInterceptors(ClassSerializerInterceptor)
export class CampaignsController {
  constructor(private readonly campaignsService: CampaignsService) {}

  @ApiOperation({
    summary: 'Listar todas las campañas registradas',
    description: 'Listar todas las campañas registradas',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
  })
  @Get()
  getCampaigns(@Query() { offset, limit, idsToSkip }: PaginationParams) {
    return this.campaignsService.getCampaigns(offset, limit, idsToSkip);
  }

  @ApiOperation({
    summary: 'Buscar una campaña por id',
    description: 'Buscar una campaña por id',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
  })
  @Get(':id')
  getCampaignById(@Param() { id }: FindOneParams) {
    return this.campaignsService.getCampaignById(id);
  }

  @ApiOperation({
    summary: 'Crear una nueva campaña',
    description: 'Crear una nueva campaña',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Success',
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
    type: ErrorBadRequestDto,
  })
  @Post()
  createCampaign(@Body() createCampaignDto: CreateCampaignDto) {
    return this.campaignsService.createCampaign(createCampaignDto);
  }
}

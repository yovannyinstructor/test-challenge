import { Test, TestingModule } from '@nestjs/testing';

import { CampaignsService } from 'campaigns/application/services/campaigns.service';
import { CampaignsController } from './campaigns.controller';

import {
  mockCampaign,
  mockCampaigns,
  mockCreateCampaignDto,
} from '../../../../test/__mocks__/campaigns/data/campaign.mock';

describe('CampaignsController test suite 🧪', () => {
  let controller: CampaignsController;
  let service: CampaignsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CampaignsController],
      providers: [
        {
          provide: CampaignsService,
          useValue: {
            getCampaigns: jest.fn(),
            getCampaignById: jest.fn(),
            createCampaign: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<CampaignsController>(CampaignsController);
    service = module.get<CampaignsService>(CampaignsService);
  });

  it('should return an array of campaigns ✅', async () => {
    jest.spyOn(service, 'getCampaigns').mockResolvedValue(mockCampaigns);

    const result = await controller.getCampaigns({});

    expect(result).toBe(mockCampaigns);
  });

  it('should return a campaign by id ✅', async () => {
    jest.spyOn(service, 'getCampaignById').mockResolvedValue(mockCampaign);

    const result = await controller.getCampaignById({ id: 1 });

    expect(result).toBe(mockCampaign);
    expect(service.getCampaignById).toHaveBeenCalledWith(1);
  });

  it('should create a new campaign ✅', async () => {
    jest.spyOn(service, 'createCampaign').mockResolvedValue(mockCampaign);

    const result = await controller.createCampaign(mockCreateCampaignDto);

    expect(result).toBe(mockCampaign);
    expect(service.createCampaign).toHaveBeenCalledWith(mockCreateCampaignDto);
  });
});

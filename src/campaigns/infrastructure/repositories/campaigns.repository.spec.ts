import { Test, TestingModule } from '@nestjs/testing';

import DatabaseService from '@shared/database/database.service';

import { CampaignsRepository } from './campaigns.repository';

import { mockCreateCampaignDto } from '../../../../test/__mocks__/campaigns/data/campaign.mock';

describe('CampaignsRepository test suite 🧪', () => {
  let repository: CampaignsRepository;
  let databaseService: DatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CampaignsRepository,
        {
          provide: DatabaseService,
          useValue: {
            runQuery: jest.fn(),
          },
        },
      ],
    }).compile();

    repository = module.get<CampaignsRepository>(CampaignsRepository);
    databaseService = module.get<DatabaseService>(DatabaseService);
  });

  it('should re-throw error if it is not a database error 🛑', async () => {
    const error = new Error('Something went wrong');

    jest.spyOn(databaseService, 'runQuery').mockRejectedValue(error);

    await expect(repository.create(mockCreateCampaignDto)).rejects.toThrowError(
      error,
    );
  });
});

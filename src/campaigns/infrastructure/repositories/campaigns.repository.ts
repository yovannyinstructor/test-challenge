import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import DatabaseService from '@shared/database/database.service';
import PostgresErrorCode from '@shared/database/postgresErrorCode.enum';
import { isDatabaseError } from '@shared/types/databaseError';

import { CreateCampaignDto } from '../../domain/dtos/create-campaign.dto';
import { CampaignModel } from '../../domain/models/campaign.model';

@Injectable()
export class CampaignsRepository {
  constructor(private readonly databaseService: DatabaseService) {}

  async get(offset = 0, limit: number | null = null, idsToSkip = 0) {
    const databaseResponse = await this.databaseService.runQuery(
      `
      WITH selected_campaigns AS (
        SELECT * FROM campaigns
        WHERE id > $3
        ORDER BY id ASC
        OFFSET $1
        LIMIT $2
      ),
      total_campaigns_count_response AS (
        SELECT COUNT(*)::int AS total_campaigns_count FROM campaigns
      )
      SELECT * FROM selected_campaigns, total_campaigns_count_response
    `,
      [offset, limit, idsToSkip],
    );
    const items = databaseResponse.rows.map(
      (databaseRow) => new CampaignModel(databaseRow),
    );
    const count = databaseResponse.rows[0]?.total_campaigns_count || 0;
    return {
      items,
      count,
    };
  }

  async getById(id: number) {
    const databaseResponse = await this.databaseService.runQuery(
      `
      SELECT * FROM campaigns WHERE id=$1
    `,
      [id],
    );
    const entity = databaseResponse.rows[0];
    if (!entity) {
      throw new NotFoundException();
    }
    return new CampaignModel(entity);
  }

  async create(createCampaignDto: CreateCampaignDto) {
    try {
      const databaseResponse = await this.databaseService.runQuery(
        `
          INSERT INTO campaigns (
            name,
            description,
            min_quantity,
            reward_factor,
            is_active,
            start_date,
            end_date,
            merchant_id
          ) VALUES (
            $1,
            $2,
            $3,
            $4,
            $5,
            $6,
            $7,
            $8
          ) RETURNING *
        `,
        [
          createCampaignDto.name,
          createCampaignDto.description,
          createCampaignDto.min_quantity,
          createCampaignDto.reward_factor,
          createCampaignDto.is_active,
          createCampaignDto.start_date,
          createCampaignDto.end_date,
          createCampaignDto.merchant_id,
        ],
      );
      return new CampaignModel(databaseResponse.rows[0]);
    } catch (error) {
      if (
        !isDatabaseError(error) ||
        ![
          'name',
          'description',
          'min_quantity',
          'reward_factor',
          'is_active',
          'start_date',
          'end_date',
          'merchant_id',
        ].includes(error.column)
      ) {
        throw error;
      }
      if (error.code === PostgresErrorCode.NotNullViolation) {
        throw new BadRequestException(
          `A null value can't be set for the ${error.column} column`,
        );
      }
      throw error;
    }
  }
}

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: 'name',
    example: 'yovanny',
  })
  readonly name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: 'email',
    example: 'yovanny@gmail.com',
  })
  readonly email: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    name: 'merchant_id',
    example: 1,
  })
  readonly merchant_id: number;
}

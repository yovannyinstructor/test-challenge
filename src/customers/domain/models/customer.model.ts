export interface CustomerModelData {
  id: number;
  name: string;
  email: string;
  merchant_id: number;
}

export class CustomerModel {
  id: number;
  name: string;
  email: string;
  merchant_id: number;

  constructor(customerData: CustomerModelData) {
    this.id = customerData.id;
    this.name = customerData.name;
    this.email = customerData.email;
    this.merchant_id = customerData.merchant_id;
  }
}

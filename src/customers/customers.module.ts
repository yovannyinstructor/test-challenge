import { Module } from '@nestjs/common';

import { CustomersService } from './application/services/customers.service';
import { CustomersController } from './infrastructure/controllers/customers.controller';
import { CustomersRepository } from './infrastructure/repositories/customers.repository';

@Module({
  imports: [],
  controllers: [CustomersController],
  providers: [CustomersService, CustomersRepository],
})
export class CustomersModule {}

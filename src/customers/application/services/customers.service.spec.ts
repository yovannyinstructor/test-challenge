import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { CustomersRepository } from 'customers/infrastructure/repositories/customers.repository';
import { CustomersService } from './customers.service';

import {
  mockCreateCustomerDto,
  mockCustomerExpectedResponse,
  mockCustomersExpectedResponse,
} from '../../../../test/__mocks__/customers/data/customer.mock';

describe('CustomersService test suite 🧪', () => {
  let service: CustomersService;
  let repository: CustomersRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CustomersService,
        {
          provide: CustomersRepository,
          useValue: {
            get: jest.fn(),
            getById: jest.fn(),
            create: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<CustomersService>(CustomersService);
    repository = module.get<CustomersRepository>(CustomersRepository);
  });

  it('should return customers with count ✅', async () => {
    jest
      .spyOn(repository, 'get')
      .mockResolvedValue(mockCustomersExpectedResponse);

    const result = await service.getCustomers();

    expect(result).toEqual(mockCustomersExpectedResponse);
  });

  it('should return a customer by id ✅', async () => {
    jest
      .spyOn(repository, 'getById')
      .mockResolvedValue(mockCustomerExpectedResponse);

    const result = await service.getCustomerById(1);

    expect(result).toEqual(mockCustomerExpectedResponse);
  });

  it('should throw NotFoundException when customer is not found 🛑', async () => {
    jest
      .spyOn(repository, 'getById')
      .mockRejectedValue(new NotFoundException());

    await expect(service.getCustomerById(1)).rejects.toThrowError(
      NotFoundException,
    );
  });

  it('should create a new customer ✅', async () => {
    jest
      .spyOn(repository, 'create')
      .mockResolvedValue(mockCustomerExpectedResponse);

    const result = await service.createCustomer(mockCreateCustomerDto);

    expect(result).toEqual(mockCustomerExpectedResponse);
  });

  it('should throw BadRequestException for invalid createCustomerDto 🛑', async () => {
    jest
      .spyOn(repository, 'create')
      .mockRejectedValue(new BadRequestException());

    await expect(
      service.createCustomer(mockCreateCustomerDto),
    ).rejects.toThrowError(BadRequestException);
  });
});

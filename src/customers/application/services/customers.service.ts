import { Injectable } from '@nestjs/common';

import { CreateCustomerDto } from 'customers/domain/dtos/create-customer.dto';
import { CustomersRepository } from 'customers/infrastructure/repositories/customers.repository';

@Injectable()
export class CustomersService {
  constructor(private readonly customersRepository: CustomersRepository) {}

  getCustomers(offset?: number, limit?: number, idsToSkip?: number) {
    return this.customersRepository.get(offset, limit, idsToSkip);
  }

  getCustomerById(id: number) {
    return this.customersRepository.getById(id);
  }

  createCustomer(createCustomerDto: CreateCustomerDto) {
    return this.customersRepository.create(createCustomerDto);
  }
}

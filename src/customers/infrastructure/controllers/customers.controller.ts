import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiOperation,
  ApiProduces,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import { ErrorBadRequestDto } from '@shared/dtos/error.dto';
import { CUSTOMERS_PATH, CUSTOMERS_TAG } from '@shared/swagger/constants';
import FindOneParams from '@shared/utils/findOneParams';
import PaginationParams from '@shared/utils/paginationParams';

import { CustomersService } from 'customers/application/services/customers.service';
import { CreateCustomerDto } from 'customers/domain/dtos/create-customer.dto';

@ApiTags(CUSTOMERS_TAG)
@Controller(CUSTOMERS_PATH)
@UseInterceptors(ClassSerializerInterceptor)
export class CustomersController {
  constructor(private readonly customersService: CustomersService) {}

  @ApiOperation({
    summary: 'Listar los clientes registrados',
    description: 'Listar los clientes registrados',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
  })
  @Get()
  getCustomers(@Query() { offset, limit, idsToSkip }: PaginationParams) {
    return this.customersService.getCustomers(offset, limit, idsToSkip);
  }

  @ApiOperation({
    summary: 'Buscar un cliente por id',
    description: 'Buscar un cliente por id',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
  })
  @Get(':id')
  getCustomerById(@Param() { id }: FindOneParams) {
    return this.customersService.getCustomerById(id);
  }

  @ApiOperation({
    summary: 'Crear un nuevo cliente',
    description: 'Crear un nuevo cliente',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Success',
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
    type: ErrorBadRequestDto,
  })
  @Post()
  createCustomer(@Body() createCustomerDto: CreateCustomerDto) {
    return this.customersService.createCustomer(createCustomerDto);
  }
}

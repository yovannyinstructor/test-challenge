import { Test, TestingModule } from '@nestjs/testing';

import { CustomersService } from 'customers/application/services/customers.service';
import { CustomersController } from './customers.controller';

import {
  mockCreateCustomerDto,
  mockCustomer,
  mockCustomers,
} from '../../../../test/__mocks__/customers/data/customer.mock';

describe('CustomersController test suite 🧪', () => {
  let controller: CustomersController;
  let service: CustomersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CustomersController],
      providers: [
        {
          provide: CustomersService,
          useValue: {
            getCustomers: jest.fn(),
            getCustomerById: jest.fn(),
            createCustomer: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<CustomersController>(CustomersController);
    service = module.get<CustomersService>(CustomersService);
  });

  it('should return an array of customers ✅', async () => {
    jest.spyOn(service, 'getCustomers').mockResolvedValue(mockCustomers);

    const result = await controller.getCustomers({});

    expect(result).toBe(mockCustomers);
  });

  it('should return a customer by id ✅', async () => {
    jest.spyOn(service, 'getCustomerById').mockResolvedValue(mockCustomer);

    const result = await controller.getCustomerById({ id: 1 });

    expect(result).toBe(mockCustomer);
    expect(service.getCustomerById).toHaveBeenCalledWith(1);
  });

  it('should create a new customer ✅', async () => {
    jest.spyOn(service, 'createCustomer').mockResolvedValue(mockCustomer);

    const result = await controller.createCustomer(mockCreateCustomerDto);

    expect(result).toBe(mockCustomer);
    expect(service.createCustomer).toHaveBeenCalledWith(mockCreateCustomerDto);
  });
});

import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import DatabaseService from '@shared/database/database.service';
import PostgresErrorCode from '@shared/database/postgresErrorCode.enum';
import { isDatabaseError } from '@shared/types/databaseError';

import { CreateCustomerDto } from 'customers/domain/dtos/create-customer.dto';
import { CustomerModel } from 'customers/domain/models/customer.model';

@Injectable()
export class CustomersRepository {
  constructor(private readonly databaseService: DatabaseService) {}

  async get(offset = 0, limit: number | null = null, idsToSkip = 0) {
    const databaseResponse = await this.databaseService.runQuery(
      `
      WITH selected_customers AS (
        SELECT * FROM customers
        WHERE id > $3
        ORDER BY id ASC
        OFFSET $1
        LIMIT $2
      ),
      total_customers_count_response AS (
        SELECT COUNT(*)::int AS total_customers_count FROM customers
      )
      SELECT * FROM selected_customers, total_customers_count_response
    `,
      [offset, limit, idsToSkip],
    );
    const items = databaseResponse.rows.map(
      (databaseRow) => new CustomerModel(databaseRow),
    );
    const count = databaseResponse.rows[0]?.total_customers_count || 0;
    return {
      items,
      count,
    };
  }

  async getById(id: number) {
    const databaseResponse = await this.databaseService.runQuery(
      `
      SELECT * FROM customers WHERE id=$1
    `,
      [id],
    );
    const entity = databaseResponse.rows[0];
    if (!entity) {
      throw new NotFoundException();
    }
    return new CustomerModel(entity);
  }

  async create(createCustomerDto: CreateCustomerDto) {
    try {
      const databaseResponse = await this.databaseService.runQuery(
        `
          INSERT INTO customers (
            name,
            email,
            merchant_id
          ) VALUES (
            $1,
            $2,
            $3
          ) RETURNING *
        `,
        [
          createCustomerDto.name,
          createCustomerDto.email,
          createCustomerDto.merchant_id,
        ],
      );
      return new CustomerModel(databaseResponse.rows[0]);
    } catch (error) {
      if (
        !isDatabaseError(error) ||
        !['name', 'email', 'merchant_id'].includes(error.column)
      ) {
        throw error;
      }
      if (error.code === PostgresErrorCode.NotNullViolation) {
        throw new BadRequestException(
          `A null value can't be set for the ${error.column} column`,
        );
      }
      throw error;
    }
  }
}

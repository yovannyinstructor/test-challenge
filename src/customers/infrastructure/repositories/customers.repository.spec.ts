import { Test, TestingModule } from '@nestjs/testing';

import DatabaseService from '@shared/database/database.service';

import { CustomersRepository } from './customers.repository';

import { mockCreateCustomerDto } from '../../../../test/__mocks__/customers/data/customer.mock';

describe('CustomersRepository test suite 🧪', () => {
  let repository: CustomersRepository;
  let databaseService: DatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CustomersRepository,
        {
          provide: DatabaseService,
          useValue: {
            runQuery: jest.fn(),
          },
        },
      ],
    }).compile();

    repository = module.get<CustomersRepository>(CustomersRepository);
    databaseService = module.get<DatabaseService>(DatabaseService);
  });

  it('should re-throw error if it is not a database error 🛑', async () => {
    const error = new Error('Something went wrong');

    jest.spyOn(databaseService, 'runQuery').mockRejectedValue(error);

    await expect(repository.create(mockCreateCustomerDto)).rejects.toThrowError(
      error,
    );
  });
});

import { Test, TestingModule } from '@nestjs/testing';

import { PurchasesService } from 'purchases/application/services/purchases.service';
import { PurchasesController } from './purchases.controller';

import {
  mockCreatePurchaseDto,
  mockPurchase,
  mockPurchases,
} from '../../../../test/__mocks__/purchases/data/purchase.mock';

describe('PurchasesController test suite 🧪', () => {
  let controller: PurchasesController;
  let service: PurchasesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PurchasesController],
      providers: [
        {
          provide: PurchasesService,
          useValue: {
            getPurchases: jest.fn(),
            getPurchaseById: jest.fn(),
            createPurchase: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<PurchasesController>(PurchasesController);
    service = module.get<PurchasesService>(PurchasesService);
  });

  it('should return an array of purchases ✅', async () => {
    jest.spyOn(service, 'getPurchases').mockResolvedValue(mockPurchases);

    const result = await controller.getPurchases({});

    expect(result).toBe(mockPurchases);
  });

  it('should return a branch by id ✅', async () => {
    jest.spyOn(service, 'getPurchaseById').mockResolvedValue(mockPurchase);

    const result = await controller.getPurchaseById({ id: 1 });

    expect(result).toBe(mockPurchase);
    expect(service.getPurchaseById).toHaveBeenCalledWith(1);
  });

  it('should create a new branch ✅', async () => {
    jest.spyOn(service, 'createPurchase').mockResolvedValue(mockPurchase);

    const result = await controller.createPurchase(mockCreatePurchaseDto);

    expect(result).toBe(mockPurchase);
    expect(service.createPurchase).toHaveBeenCalledWith(mockCreatePurchaseDto);
  });
});

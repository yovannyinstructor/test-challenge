import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiOperation,
  ApiProduces,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import { ErrorBadRequestDto } from '@shared/dtos/error.dto';
import { PURCHASES_PATH, PURCHASES_TAG } from '@shared/swagger/constants';
import FindOneParams from '@shared/utils/findOneParams';
import PaginationParams from '@shared/utils/paginationParams';

import { PurchasesService } from '../../application/services/purchases.service';
import { CreatePurchaseDto } from '../../domain/dtos/create-purchase.dto';

@ApiTags(PURCHASES_TAG)
@Controller(PURCHASES_PATH)
@UseInterceptors(ClassSerializerInterceptor)
export class PurchasesController {
  constructor(private readonly purchasesService: PurchasesService) {}

  @ApiOperation({
    summary: 'Listar todas las campañas registradas',
    description: 'Listar todas las campañas registradas',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
  })
  @Get()
  getPurchases(@Query() { offset, limit, idsToSkip }: PaginationParams) {
    return this.purchasesService.getPurchases(offset, limit, idsToSkip);
  }

  @ApiOperation({
    summary: 'Buscar una campaña por id',
    description: 'Buscar una campaña por id',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
  })
  @Get(':id')
  getPurchaseById(@Param() { id }: FindOneParams) {
    return this.purchasesService.getPurchaseById(id);
  }

  @ApiOperation({
    summary: 'Crear una nueva campaña',
    description: 'Crear una nueva campaña',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Success',
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
    type: ErrorBadRequestDto,
  })
  @Post()
  createPurchase(@Body() createPurchaseDto: CreatePurchaseDto) {
    return this.purchasesService.createPurchase(createPurchaseDto);
  }
}

import { Test, TestingModule } from '@nestjs/testing';

import DatabaseService from '@shared/database/database.service';

import { PurchasesRepository } from './purchases.repository';

import { mockCreatePurchaseDto } from '../../../../test/__mocks__/purchases/data/purchase.mock';

describe('PurchasesRepository test suite 🧪', () => {
  let repository: PurchasesRepository;
  let databaseService: DatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PurchasesRepository,
        {
          provide: DatabaseService,
          useValue: {
            runQuery: jest.fn(),
          },
        },
      ],
    }).compile();

    repository = module.get<PurchasesRepository>(PurchasesRepository);
    databaseService = module.get<DatabaseService>(DatabaseService);
  });

  it('should re-throw error if it is not a database error 🛑', async () => {
    const error = new Error('Something went wrong');

    jest.spyOn(databaseService, 'runQuery').mockRejectedValue(error);

    await expect(repository.create(mockCreatePurchaseDto)).rejects.toThrowError(
      error,
    );
  });
});

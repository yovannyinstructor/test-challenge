import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import DatabaseService from '@shared/database/database.service';
import PostgresErrorCode from '@shared/database/postgresErrorCode.enum';
import { isDatabaseError } from '@shared/types/databaseError';

import { CreatePurchaseDto } from '../../domain/dtos/create-purchase.dto';
import { PurchaseModel } from '../../domain/models/purchase.model';

@Injectable()
export class PurchasesRepository {
  constructor(private readonly databaseService: DatabaseService) {}

  async get(offset = 0, limit: number | null = null, idsToSkip = 0) {
    const databaseResponse = await this.databaseService.runQuery(
      `
      WITH selected_purchases AS (
        SELECT * FROM purchases
        WHERE id > $3
        ORDER BY id ASC
        OFFSET $1
        LIMIT $2
      ),
      total_purchases_count_response AS (
        SELECT COUNT(*)::int AS total_purchases_count FROM purchases
      )
      SELECT * FROM selected_purchases, total_purchases_count_response
    `,
      [offset, limit, idsToSkip],
    );
    const items = databaseResponse.rows.map(
      (databaseRow) => new PurchaseModel(databaseRow),
    );
    const count = databaseResponse.rows[0]?.total_purchases_count || 0;
    return {
      items,
      count,
    };
  }

  async getById(id: number) {
    const databaseResponse = await this.databaseService.runQuery(
      `
      SELECT * FROM purchases WHERE id=$1
    `,
      [id],
    );
    const entity = databaseResponse.rows[0];
    if (!entity) {
      throw new NotFoundException();
    }
    return new PurchaseModel(entity);
  }

  async create(createPurchaseData: CreatePurchaseDto) {
    try {
      const databaseResponse = await this.databaseService.runQuery(
        `
          INSERT INTO purchases (
            amount,
            branch_id,
            campaign_id,
            customer_id
          ) VALUES (
            $1,
            $2,
            $3,
            $4
          ) RETURNING *
        `,
        [
          createPurchaseData.amount,
          createPurchaseData.branch_id,
          createPurchaseData.campaign_id,
          createPurchaseData.customer_id,
        ],
      );
      return new PurchaseModel(databaseResponse.rows[0]);
    } catch (error) {
      if (
        !isDatabaseError(error) ||
        !['amount', 'branch_id', 'campaign_id', 'customer_id'].includes(
          error.column,
        )
      ) {
        throw error;
      }
      if (error.code === PostgresErrorCode.NotNullViolation) {
        throw new BadRequestException(
          `A null value can't be set for the ${error.column} column`,
        );
      }
      throw error;
    }
  }
}

export interface PurchaseModelData {
  id: number;
  amount: number;
  branch_id: number;
  campaign_id: number;
  customer_id: number;
}

export class PurchaseModel {
  id: number;
  amount: number;
  branch_id: number;
  campaign_id: number;
  customer_id: number;

  constructor(purchaseData: PurchaseModelData) {
    this.id = purchaseData.id;
    this.amount = purchaseData.amount;
    this.branch_id = purchaseData.branch_id;
    this.campaign_id = purchaseData.campaign_id;
    this.customer_id = purchaseData.customer_id;
  }
}

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreatePurchaseDto {
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    name: 'amount',
    example: 1000,
  })
  readonly amount: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    name: 'campaign_id',
    example: 1,
  })
  readonly campaign_id: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    name: 'branch_id',
    example: 1,
  })
  readonly branch_id: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    name: 'customer_id',
    example: 1,
  })
  readonly customer_id: number;
}

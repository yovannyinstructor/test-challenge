import { Injectable } from '@nestjs/common';

import { RewardsService } from 'rewards/application/services/rewards.service';
import { CreatePurchaseDto } from '../../domain/dtos/create-purchase.dto';
import { PurchasesRepository } from '../../infrastructure/repositories/purchases.repository';

@Injectable()
export class PurchasesService {
  constructor(
    private readonly purchasesRepository: PurchasesRepository,
    private readonly rewardsService: RewardsService,
  ) {}

  getPurchases(offset?: number, limit?: number, idsToSkip?: number) {
    return this.purchasesRepository.get(offset, limit, idsToSkip);
  }

  getPurchaseById(id: number) {
    return this.purchasesRepository.getById(id);
  }

  createPurchase(createPurchaseDto: CreatePurchaseDto) {
    this.rewardsService.calculateRewards(createPurchaseDto);
    return this.purchasesRepository.create(createPurchaseDto);
  }
}

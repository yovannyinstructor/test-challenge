import { Test } from '@nestjs/testing';

import DatabaseService from '@shared/database/database.service';

import { PurchasesService } from './purchases.service';
import { PurchasesRepository } from '../../infrastructure/repositories/purchases.repository';
import { RewardsService } from 'rewards/application/services/rewards.service';
import { RewardsRepository } from 'rewards/infrastructure/repositories/rewards.repository';
import { CampaignsRepository } from 'campaigns/infrastructure/repositories/campaigns.repository';

import {
  mockCreatePurchaseDto,
  mockPurchase,
  mockPurchases,
} from '../../../../test/__mocks__/purchases/data/purchase.mock';

describe('PurchasesService test suite 🧪', () => {
  let purchasesService: PurchasesService;
  let purchasesRepository: PurchasesRepository;
  let rewardsService: RewardsService;
  let rewardsRepository: RewardsRepository;
  let campaignsRepository: CampaignsRepository;
  let databaseService: DatabaseService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        PurchasesService,
        PurchasesRepository,
        RewardsService,
        {
          provide: RewardsRepository,
          useValue: {
            get: jest.fn(),
            getById: jest.fn(),
            create: jest.fn(),
          },
        },
        {
          provide: CampaignsRepository,
          useValue: {
            get: jest.fn(),
            getById: jest.fn(),
            create: jest.fn(),
          },
        },
        {
          provide: DatabaseService,
          useValue: {
            runQuery: jest.fn(),
          },
        },
      ],
    }).compile();

    purchasesService = moduleRef.get<PurchasesService>(PurchasesService);
    purchasesRepository =
      moduleRef.get<PurchasesRepository>(PurchasesRepository);
    rewardsService = moduleRef.get<RewardsService>(RewardsService);
    rewardsRepository = moduleRef.get<RewardsRepository>(RewardsRepository);
    campaignsRepository =
      moduleRef.get<CampaignsRepository>(CampaignsRepository);
  });

  it('should call the purchasesRepository.get method with the correct parameters ✅', () => {
    const offset = 0;
    const limit = 10;
    const idsToSkip = 5;
    jest.spyOn(purchasesRepository, 'get').mockResolvedValueOnce(mockPurchases);

    purchasesService.getPurchases(offset, limit, idsToSkip);

    expect(purchasesRepository.get).toHaveBeenCalledWith(
      offset,
      limit,
      idsToSkip,
    );
  });

  it('should return the purchases from the purchasesRepository ✅', async () => {
    jest.spyOn(purchasesRepository, 'get').mockResolvedValueOnce(mockPurchases);

    const result = await purchasesService.getPurchases();

    expect(result).toEqual(mockPurchases);
  });

  it('should return the purchase from the purchasesRepository ✅', async () => {
    const purchaseId = 1;
    jest
      .spyOn(purchasesRepository, 'getById')
      .mockResolvedValueOnce(mockPurchase);

    const result = await purchasesService.getPurchaseById(purchaseId);

    expect(result).toEqual(mockPurchase);
  });

  it('should call the purchasesRepository.create method with the correct parameter ✅', async () => {
    const purchase = {
      id: 1,
      amount: 1000,
      branch_id: 1,
      campaign_id: 1,
      customer_id: 1,
    };
    jest.spyOn(rewardsService, 'calculateRewards').mockImplementation();
    jest.spyOn(purchasesRepository, 'create').mockResolvedValueOnce(purchase);

    const result = await purchasesService.createPurchase(mockCreatePurchaseDto);

    expect(purchasesRepository.create).toHaveBeenCalledWith(
      mockCreatePurchaseDto,
    );
    expect(result).toEqual(purchase);
  });
});

import { Module } from '@nestjs/common';

import { CampaignsService } from 'campaigns/application/services/campaigns.service';
import { CampaignsRepository } from 'campaigns/infrastructure/repositories/campaigns.repository';
import { RewardsService } from 'rewards/application/services/rewards.service';
import { RewardsRepository } from 'rewards/infrastructure/repositories/rewards.repository';
import { PurchasesService } from './application/services/purchases.service';
import { PurchasesController } from './infrastructure/controllers/purchases.controller';
import { PurchasesRepository } from './infrastructure/repositories/purchases.repository';

@Module({
  imports: [],
  controllers: [PurchasesController],
  providers: [
    CampaignsService,
    CampaignsRepository,
    PurchasesService,
    PurchasesRepository,
    RewardsService,
    RewardsRepository,
  ],
})
export class PurchasesModule {}

import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { MerchantsRepository } from 'merchants/infrastructure/repositories/merchants.repository';
import { MerchantsService } from './merchants.service';

import {
  mockCreateMerchantDto,
  mockMerchantExpectedResponse,
  mockMerchantsExpectedResponse,
} from '../../../../test/__mocks__/merchants/data/merchant.mock';

describe('MerchantsService test suite 🧪', () => {
  let service: MerchantsService;
  let repository: MerchantsRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MerchantsService,
        {
          provide: MerchantsRepository,
          useValue: {
            get: jest.fn(),
            getById: jest.fn(),
            create: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<MerchantsService>(MerchantsService);
    repository = module.get<MerchantsRepository>(MerchantsRepository);
  });

  it('should return merchants with count ✅', async () => {
    jest
      .spyOn(repository, 'get')
      .mockResolvedValue(mockMerchantsExpectedResponse);

    const result = await service.getMerchants();

    expect(result).toEqual(mockMerchantsExpectedResponse);
  });

  it('should return a merchants by id ✅', async () => {
    jest
      .spyOn(repository, 'getById')
      .mockResolvedValue(mockMerchantExpectedResponse);

    const result = await service.getMerchantById(1);

    expect(result).toEqual(mockMerchantExpectedResponse);
  });

  it('should throw NotFoundException when merchants is not found 🛑', async () => {
    jest
      .spyOn(repository, 'getById')
      .mockRejectedValue(new NotFoundException());

    await expect(service.getMerchantById(1)).rejects.toThrowError(
      NotFoundException,
    );
  });

  it('should create a new merchants ✅', async () => {
    jest
      .spyOn(repository, 'create')
      .mockResolvedValue(mockMerchantExpectedResponse);

    const result = await service.createMerchant(mockCreateMerchantDto);

    expect(result).toEqual(mockMerchantExpectedResponse);
  });

  it('should throw BadRequestException for invalid createMerchantDto 🛑', async () => {
    jest
      .spyOn(repository, 'create')
      .mockRejectedValue(new BadRequestException());

    await expect(
      service.createMerchant(mockCreateMerchantDto),
    ).rejects.toThrowError(BadRequestException);
  });
});

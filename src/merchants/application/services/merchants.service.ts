import { Injectable } from '@nestjs/common';

import { CreateMerchantDto } from 'merchants/domain/dtos/create-merchant.dto';
import { MerchantsRepository } from 'merchants/infrastructure/repositories/merchants.repository';

@Injectable()
export class MerchantsService {
  constructor(private readonly merchantsRepository: MerchantsRepository) {}

  getMerchants(offset?: number, limit?: number, idsToSkip?: number) {
    return this.merchantsRepository.get(offset, limit, idsToSkip);
  }

  getMerchantById(id: number) {
    return this.merchantsRepository.getById(id);
  }

  createMerchant(createMerchantDto: CreateMerchantDto) {
    return this.merchantsRepository.create(createMerchantDto);
  }
}

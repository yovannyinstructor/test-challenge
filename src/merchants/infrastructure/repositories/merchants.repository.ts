import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import DatabaseService from '@shared/database/database.service';
import PostgresErrorCode from '@shared/database/postgresErrorCode.enum';
import { isDatabaseError } from '@shared/types/databaseError';

import { CreateMerchantDto } from 'merchants/domain/dtos/create-merchant.dto';
import { MerchantModel } from 'merchants/domain/models/merchant.model';

@Injectable()
export class MerchantsRepository {
  constructor(private readonly databaseService: DatabaseService) {}

  async get(offset = 0, limit: number | null = null, idsToSkip = 0) {
    const databaseResponse = await this.databaseService.runQuery(
      `
      WITH selected_merchants AS (
        SELECT * FROM merchants
        WHERE id > $3
        ORDER BY id ASC
        OFFSET $1
        LIMIT $2
      ),
      total_merchants_count_response AS (
        SELECT COUNT(*)::int AS total_merchants_count FROM merchants
      )
      SELECT * FROM selected_merchants, total_merchants_count_response
    `,
      [offset, limit, idsToSkip],
    );
    const items = databaseResponse.rows.map(
      (databaseRow) => new MerchantModel(databaseRow),
    );
    const count = databaseResponse.rows[0]?.total_merchants_count || 0;
    return {
      items,
      count,
    };
  }

  async getById(id: number) {
    const databaseResponse = await this.databaseService.runQuery(
      `
      SELECT * FROM merchants WHERE id=$1
    `,
      [id],
    );
    const entity = databaseResponse.rows[0];
    if (!entity) {
      throw new NotFoundException();
    }
    return new MerchantModel(entity);
  }

  async create(createMerchantDto: CreateMerchantDto) {
    try {
      const databaseResponse = await this.databaseService.runQuery(
        `
          INSERT INTO merchants (
            name,
            nit
          ) VALUES (
            $1,
            $2
          ) RETURNING *
        `,
        [createMerchantDto.name, createMerchantDto.nit],
      );
      return new MerchantModel(databaseResponse.rows[0]);
    } catch (error) {
      if (!isDatabaseError(error) || !['name', 'nit'].includes(error.column)) {
        throw error;
      }
      if (error.code === PostgresErrorCode.NotNullViolation) {
        throw new BadRequestException(
          `A null value can't be set for the ${error.column} column`,
        );
      }
      throw error;
    }
  }
}

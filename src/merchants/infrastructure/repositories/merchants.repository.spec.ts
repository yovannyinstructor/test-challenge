import { Test, TestingModule } from '@nestjs/testing';

import DatabaseService from '@shared/database/database.service';

import { MerchantsRepository } from './merchants.repository';

import { mockCreateMerchantDto } from '../../../../test/__mocks__/merchants/data/merchant.mock';

describe('MerchantsRepository test suite 🧪', () => {
  let repository: MerchantsRepository;
  let databaseService: DatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MerchantsRepository,
        {
          provide: DatabaseService,
          useValue: {
            runQuery: jest.fn(),
          },
        },
      ],
    }).compile();

    repository = module.get<MerchantsRepository>(MerchantsRepository);
    databaseService = module.get<DatabaseService>(DatabaseService);
  });

  it('should re-throw error if it is not a database error 🛑', async () => {
    const error = new Error('Something went wrong');

    jest.spyOn(databaseService, 'runQuery').mockRejectedValue(error);

    await expect(repository.create(mockCreateMerchantDto)).rejects.toThrowError(
      error,
    );
  });
});

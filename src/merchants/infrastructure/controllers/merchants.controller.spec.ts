import { Test, TestingModule } from '@nestjs/testing';

import { MerchantsController } from './merchants.controller';
import { MerchantsService } from 'merchants/application/services/merchants.service';

import {
  mockCreateMerchantDto,
  mockMerchant,
  mockMerchants,
} from '../../../../test/__mocks__/merchants/data/merchant.mock';

describe('MerchantsController test suite 🧪', () => {
  let controller: MerchantsController;
  let service: MerchantsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MerchantsController],
      providers: [
        {
          provide: MerchantsService,
          useValue: {
            getMerchants: jest.fn(),
            getMerchantById: jest.fn(),
            createMerchant: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<MerchantsController>(MerchantsController);
    service = module.get<MerchantsService>(MerchantsService);
  });

  /* it('should return an array of merchants ✅', async () => {
    jest.spyOn(service, 'getMerchants').mockResolvedValue(mockMerchants);

    const result = await controller.getMerchants({});

    expect(result).toBe(mockMerchants);
  }); */

  it('should return a merchant by id ✅', async () => {
    jest.spyOn(service, 'getMerchantById').mockResolvedValue(mockMerchant);

    const result = await controller.getMerchantById({ id: 1 });

    expect(result).toBe(mockMerchant);
    expect(service.getMerchantById).toHaveBeenCalledWith(1);
  });

  it('should create a new merchant ✅', async () => {
    jest.spyOn(service, 'createMerchant').mockResolvedValue(mockMerchant);

    const result = await controller.createMerchant(mockCreateMerchantDto);

    expect(result).toBe(mockMerchant);
    expect(service.createMerchant).toHaveBeenCalledWith(mockCreateMerchantDto);
  });
});

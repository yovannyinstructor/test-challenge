import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiOperation,
  ApiProduces,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import { ErrorBadRequestDto } from '@shared/dtos/error.dto';
import { MERCHANTS_PATH, MERCHANTS_TAG } from '@shared/swagger/constants';
import FindOneParams from '@shared/utils/findOneParams';
import PaginationParams from '@shared/utils/paginationParams';

import { MerchantsService } from 'merchants/application/services/merchants.service';
import { CreateMerchantDto } from 'merchants/domain/dtos/create-merchant.dto';

@ApiTags(MERCHANTS_TAG)
@Controller(MERCHANTS_PATH)
@UseInterceptors(ClassSerializerInterceptor)
export class MerchantsController {
  constructor(private readonly merchantsService: MerchantsService) {}

  @ApiOperation({
    summary: 'Listar todos los comercios registrados',
    description: 'Listar todos los comercios registrados',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
  })
  @Get()
  getMerchants(@Query() { offset, limit, idsToSkip }: PaginationParams) {
    return this.merchantsService.getMerchants(offset, limit, idsToSkip);
  }

  @ApiOperation({
    summary: 'Buscar un comercio por id',
    description: 'Buscar un comercio por id',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
  })
  @Get(':id')
  getMerchantById(@Param() { id }: FindOneParams) {
    return this.merchantsService.getMerchantById(id);
  }

  @ApiOperation({
    summary: 'Crear un nuevo comercio',
    description: 'Crear un nuevo comercio',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Success',
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
    type: ErrorBadRequestDto,
  })
  @Post()
  createMerchant(@Body() createMerchantDto: CreateMerchantDto) {
    return this.merchantsService.createMerchant(createMerchantDto);
  }
}

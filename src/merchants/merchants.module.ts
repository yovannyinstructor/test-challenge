import { Module } from '@nestjs/common';

import { MerchantsService } from './application/services/merchants.service';
import { MerchantsController } from './infrastructure/controllers/merchants.controller';
import { MerchantsRepository } from './infrastructure/repositories/merchants.repository';

@Module({
  imports: [],
  controllers: [MerchantsController],
  providers: [MerchantsService, MerchantsRepository],
})
export class MerchantsModule {}

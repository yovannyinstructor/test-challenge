import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateMerchantDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    name: 'name',
    example: 'texaco',
  })
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    name: 'nit',
    example: '9138000-1',
  })
  readonly nit: string;
}

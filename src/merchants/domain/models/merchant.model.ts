export interface MerchantModelData {
  id: number;
  name: string;
  nit: string;
}

export class MerchantModel {
  id: number;
  name: string;
  nit: string;

  constructor(merchantData: MerchantModelData) {
    this.id = merchantData.id;
    this.name = merchantData.name;
    this.nit = merchantData.nit;
  }
}

import { NestFactory } from '@nestjs/core';
import { LogLevel, Logger } from '@nestjs/common/services/logger.service';
import { ConfigService } from '@nestjs/config';

import { AppModule } from './app.module';
import { SwaggerConfig } from '@shared/swagger/config';
import { urlEncoded } from '@shared/middlewares/urlEncoded.middleware';
import { globalPipe } from '@shared/pipes/global.pipe';
import { HttpExceptionFilter } from '@shared/interceptors/httpException.filter';

async function bootstrap() {
  const isProduction = process.env.NODE_ENV === 'production';
  const logLevels: LogLevel[] = isProduction
    ? ['error', 'warn', 'log']
    : ['error', 'warn', 'log', 'verbose', 'debug'];

  const app = await NestFactory.create(AppModule, {
    logger: logLevels,
  });

  app.get(ConfigService);
  const configService = app.get(ConfigService);
  const port = configService.get('HTTP_PORT') || 3000;

  // Middleware's
  urlEncoded(app);

  // Pipes
  globalPipe(app);

  // Swagger
  if (process.env.ENVIRONMENT !== 'production') {
    const swagger = new SwaggerConfig();
    swagger.setup(app);
  }

  // Filters
  app.useGlobalFilters(new HttpExceptionFilter());

  await app.listen(port);

  Logger.log(`🚀 Application is running on: http://localhost:${port}`);
}
bootstrap();

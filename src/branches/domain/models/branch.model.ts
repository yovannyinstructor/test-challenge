export interface BranchModelData {
  id: number;
  name: string;
  merchant_id: number;
}

export class BranchModel {
  id: number;
  name: string;
  merchant_id: number;

  constructor(branchData: BranchModelData) {
    this.id = branchData.id;
    this.name = branchData.name;
    this.merchant_id = branchData.merchant_id;
  }
}

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateBranchDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: 'name',
    example: 'sucursal 1',
  })
  readonly name: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    name: 'merchant_id',
    example: 1,
  })
  readonly merchant_id: number;
}

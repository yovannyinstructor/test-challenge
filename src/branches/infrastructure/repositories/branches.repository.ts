import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import DatabaseService from '@shared/database/database.service';
import PostgresErrorCode from '@shared/database/postgresErrorCode.enum';
import { isDatabaseError } from '@shared/types/databaseError';

import { CreateBranchDto } from 'branches/domain/dtos/create-branch.dto';
import { BranchModel } from 'branches/domain/models/branch.model';

@Injectable()
export class BranchesRepository {
  constructor(private readonly databaseService: DatabaseService) {}

  async get(offset = 0, limit: number | null = null, idsToSkip = 0) {
    const databaseResponse = await this.databaseService.runQuery(
      `
      WITH selected_branches AS (
        SELECT * FROM branches
        WHERE id > $3
        ORDER BY id ASC
        OFFSET $1
        LIMIT $2
      ),
      total_branches_count_response AS (
        SELECT COUNT(*)::int AS total_branches_count FROM branches
      )
      SELECT * FROM selected_branches, total_branches_count_response
    `,
      [offset, limit, idsToSkip],
    );
    const items = databaseResponse.rows.map(
      (databaseRow) => new BranchModel(databaseRow),
    );
    const count = databaseResponse.rows[0]?.total_branches_count || 0;
    return {
      items,
      count,
    };
  }

  async getById(id: number) {
    const databaseResponse = await this.databaseService.runQuery(
      `
      SELECT * FROM branches WHERE id=$1
    `,
      [id],
    );
    const entity = databaseResponse.rows[0];
    if (!entity) {
      throw new NotFoundException();
    }
    return new BranchModel(entity);
  }

  async create(createBranchDto: CreateBranchDto) {
    try {
      const databaseResponse = await this.databaseService.runQuery(
        `
          INSERT INTO branches (
            name,
            merchant_id
          ) VALUES (
            $1,
            $2
          ) RETURNING *
        `,
        [createBranchDto.name, createBranchDto.merchant_id],
      );
      return new BranchModel(databaseResponse.rows[0]);
    } catch (error) {
      if (
        !isDatabaseError(error) ||
        !['name', 'merchant_id'].includes(error.column)
      ) {
        throw error;
      }
      if (error.code === PostgresErrorCode.NotNullViolation) {
        throw new BadRequestException(
          `A null value can't be set for the ${error.column} column`,
        );
      }
      throw error;
    }
  }
}

import { Test, TestingModule } from '@nestjs/testing';

import DatabaseService from '@shared/database/database.service';

import { BranchesRepository } from './branches.repository';

import { mockCreateBranchDto } from '../../../../test/__mocks__/branches/data/branch.mock';

describe('BranchesRepository test suite 🧪', () => {
  let repository: BranchesRepository;
  let databaseService: DatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BranchesRepository,
        {
          provide: DatabaseService,
          useValue: {
            runQuery: jest.fn(),
          },
        },
      ],
    }).compile();

    repository = module.get<BranchesRepository>(BranchesRepository);
    databaseService = module.get<DatabaseService>(DatabaseService);
  });

  it('should re-throw error if it is not a database error', async () => {
    const error = new Error('Something went wrong');

    jest.spyOn(databaseService, 'runQuery').mockRejectedValue(error);

    await expect(repository.create(mockCreateBranchDto)).rejects.toThrowError(
      error,
    );
  });
});

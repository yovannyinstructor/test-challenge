import { Test, TestingModule } from '@nestjs/testing';

import { BranchesService } from 'branches/application/services/branches.service';
import { BranchesController } from './branches.controller';

import {
  mockBranch,
  mockBranches,
  mockCreateBranchDto,
} from '../../../../test/__mocks__/branches/data/branch.mock';

describe('BranchesController test suite 🧪', () => {
  let controller: BranchesController;
  let service: BranchesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BranchesController],
      providers: [
        {
          provide: BranchesService,
          useValue: {
            getBranches: jest.fn(),
            getBranchById: jest.fn(),
            createBranch: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<BranchesController>(BranchesController);
    service = module.get<BranchesService>(BranchesService);
  });

  it('should return an array of branches ✅', async () => {
    jest.spyOn(service, 'getBranches').mockResolvedValue(mockBranches);

    const result = await controller.getBranches({});

    expect(result).toBe(mockBranches);
  });

  it('should return a branch by id ✅', async () => {
    jest.spyOn(service, 'getBranchById').mockResolvedValue(mockBranch);

    const result = await controller.getBranchById({ id: 1 });

    expect(result).toBe(mockBranch);
    expect(service.getBranchById).toHaveBeenCalledWith(1);
  });

  it('should create a new branch ✅', async () => {
    jest.spyOn(service, 'createBranch').mockResolvedValue(mockBranch);

    const result = await controller.createBranch(mockCreateBranchDto);

    expect(result).toBe(mockBranch);
    expect(service.createBranch).toHaveBeenCalledWith(mockCreateBranchDto);
  });
});

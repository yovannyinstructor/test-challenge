import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiOperation,
  ApiProduces,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import { ErrorBadRequestDto } from '@shared/dtos/error.dto';
import { BRANCHES_PATH, BRANCHES_TAG } from '@shared/swagger/constants';
import FindOneParams from '@shared/utils/findOneParams';
import PaginationParams from '@shared/utils/paginationParams';

import { BranchesService } from 'branches/application/services/branches.service';
import { CreateBranchDto } from 'branches/domain/dtos/create-branch.dto';

@ApiTags(BRANCHES_TAG)
@Controller(BRANCHES_PATH)
@UseInterceptors(ClassSerializerInterceptor)
export class BranchesController {
  constructor(private readonly branchesService: BranchesService) {}

  @ApiOperation({
    summary: 'Listar todas las sucursales registradas',
    description: 'Listar todas las sucursales registradas',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
  })
  @Get()
  getBranches(@Query() { offset, limit, idsToSkip }: PaginationParams) {
    return this.branchesService.getBranches(offset, limit, idsToSkip);
  }

  @ApiOperation({
    summary: 'Buscar una sucursal por id',
    description: 'Buscar una sucursal por id',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
  })
  @Get(':id')
  getBranchById(@Param() { id }: FindOneParams) {
    return this.branchesService.getBranchById(id);
  }

  @ApiOperation({
    summary: 'Crear una nueva sucursal',
    description: 'Crear una nueva sucursal',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Success',
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
    type: ErrorBadRequestDto,
  })
  @Post()
  createBranch(@Body() createBranchDto: CreateBranchDto) {
    return this.branchesService.createBranch(createBranchDto);
  }
}

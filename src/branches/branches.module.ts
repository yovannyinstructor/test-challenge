import { Module } from '@nestjs/common';

import { BranchesService } from './application/services/branches.service';
import { BranchesController } from './infrastructure/controllers/branches.controller';
import { BranchesRepository } from './infrastructure/repositories/branches.repository';

@Module({
  imports: [],
  controllers: [BranchesController],
  providers: [BranchesService, BranchesRepository],
})
export class BranchesModule {}

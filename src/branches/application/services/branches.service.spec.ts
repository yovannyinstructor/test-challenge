import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { BranchesRepository } from 'branches/infrastructure/repositories/branches.repository';
import { BranchesService } from './branches.service';

import {
  mockBranchExpectedResponse,
  mockBranchesExpectedResponse,
  mockCreateBranchDto,
} from '../../../../test/__mocks__/branches/data/branch.mock';

describe('BranchesService test suite 🧪', () => {
  let service: BranchesService;
  let repository: BranchesRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BranchesService,
        {
          provide: BranchesRepository,
          useValue: {
            get: jest.fn(),
            getById: jest.fn(),
            create: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<BranchesService>(BranchesService);
    repository = module.get<BranchesRepository>(BranchesRepository);
  });

  it('should return branches with count ✅', async () => {
    jest
      .spyOn(repository, 'get')
      .mockResolvedValue(mockBranchesExpectedResponse);

    const result = await service.getBranches();

    expect(result).toEqual(mockBranchesExpectedResponse);
  });

  it('should return a branch by id ✅', async () => {
    jest
      .spyOn(repository, 'getById')
      .mockResolvedValue(mockBranchExpectedResponse);

    const result = await service.getBranchById(1);

    expect(result).toEqual(mockBranchExpectedResponse);
  });

  it('should throw NotFoundException when branch is not found 🛑', async () => {
    jest
      .spyOn(repository, 'getById')
      .mockRejectedValue(new NotFoundException());

    await expect(service.getBranchById(1)).rejects.toThrowError(
      NotFoundException,
    );
  });

  it('should create a new branch ✅', async () => {
    jest
      .spyOn(repository, 'create')
      .mockResolvedValue(mockBranchExpectedResponse);

    const result = await service.createBranch(mockCreateBranchDto);

    expect(result).toEqual(mockBranchExpectedResponse);
  });

  it('should throw BadRequestException for invalid createBranchDto 🛑', async () => {
    jest
      .spyOn(repository, 'create')
      .mockRejectedValue(new BadRequestException());

    await expect(
      service.createBranch(mockCreateBranchDto),
    ).rejects.toThrowError(BadRequestException);
  });
});

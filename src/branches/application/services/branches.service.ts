import { Injectable } from '@nestjs/common';

import { CreateBranchDto } from 'branches/domain/dtos/create-branch.dto';
import { BranchesRepository } from 'branches/infrastructure/repositories/branches.repository';

@Injectable()
export class BranchesService {
  constructor(private readonly branchesRepository: BranchesRepository) {}

  getBranches(offset?: number, limit?: number, idsToSkip?: number) {
    return this.branchesRepository.get(offset, limit, idsToSkip);
  }

  getBranchById(id: number) {
    return this.branchesRepository.getById(id);
  }

  createBranch(createBranchDto: CreateBranchDto) {
    return this.branchesRepository.create(createBranchDto);
  }
}

/* eslint-disable @typescript-eslint/no-var-requires */
import { join, resolve } from 'path';

export const packageJson = require(join(resolve('./'), 'package.json'));

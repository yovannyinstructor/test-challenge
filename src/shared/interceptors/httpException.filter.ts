import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  Logger,
} from '@nestjs/common';
import { Request, Response } from 'express';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  private readonly logger = new Logger(HttpExceptionFilter.name);

  catch(exception: HttpException, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse<Response>();
    let status = 500;
    let responseAux: string | any = 'Internal Error';
    const { method, url, params, body, headers } = request;

    try {
      status = exception.getStatus();
      responseAux = exception.getResponse();

      if (typeof responseAux === 'string') {
        responseAux = {
          message: responseAux,
          statusCode: status,
          error: 'Service Error',
        };
      }
    } catch (error) {
      status = 500;
      responseAux = {
        message: 'Service error',
        statusCode: status,
        error: 'Service error',
      };
    }

    const errorAux = exception.message;

    if ([401, 403, 400, 404, 406, 409].includes(status)) {
      this.logger.warn({
        endpointType: method,
        endpointUrl: url,
        status,
        request: {
          params,
          body,
          headers,
        },
        response: responseAux,
        error: errorAux,
      });
    } else {
      this.logger.error({
        endpointType: method,
        endpointUrl: url,
        status,
        request: {
          params,
          body,
          headers,
        },
        response: responseAux,
        error: errorAux,
      });
    }

    response.status(status).json(responseAux);
  }
}

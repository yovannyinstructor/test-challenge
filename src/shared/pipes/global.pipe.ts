import { ValidationPipe } from '@nestjs/common';

export const globalPipe = (app: any): void => {
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
    }),
  );
};

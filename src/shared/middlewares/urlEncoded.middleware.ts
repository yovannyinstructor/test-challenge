import { json, urlencoded } from 'express';
import helmet from 'helmet';

export const urlEncoded = (app: any): void => {
  app.use(helmet());
  app.use(json({ limit: process.env.JSON_REQUEST_LIMIT }));
  app.use(urlencoded({ limit: process.env.URL_REQUEST_LIMIT, extended: true }));
};

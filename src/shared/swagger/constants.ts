import { packageJson } from '../utils/packageJson.util';

export const SWAGGER = {
  DESCRIPTION: packageJson.description,
  TITLE: packageJson.name,
  VERSION: packageJson.version,
};

// tags
export const CAMPAIGNS_TAG = 'Campañas';
export const CUSTOMERS_TAG = 'Clientes';
export const MERCHANTS_TAG = 'Comercios';
export const PURCHASES_TAG = 'Compras';
export const REWARDS_TAG = 'Puntos/Premios';
export const BRANCHES_TAG = 'Sucursales';
export const HEALTH_TAG = 'Health';

// paths
export const BRANCHES_PATH = 'branches';
export const CAMPAIGNS_PATH = 'campaigns';
export const CUSTOMERS_PATH = 'customers';
export const REWARDS_PATH = 'rewards';
export const MERCHANTS_PATH = 'merchants';
export const SWAGGER_PATH = 'swagger';
export const PURCHASES_PATH = 'purchases';

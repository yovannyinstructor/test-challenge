import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from '@nestjs/swagger';

import { SWAGGER, SWAGGER_PATH } from './constants';

export class SwaggerConfig {
  swagger: Omit<OpenAPIObject, 'paths'>;

  constructor() {
    this.swagger = new DocumentBuilder()
      .setDescription(SWAGGER.DESCRIPTION)
      .setTitle(SWAGGER.TITLE)
      .setVersion(SWAGGER.VERSION)
      .build();
  }

  setup(app: INestApplication): void {
    const document = SwaggerModule.createDocument(app, this.swagger);
    SwaggerModule.setup(SWAGGER_PATH, app, document);
  }
}

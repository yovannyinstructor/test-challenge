import { ApiProperty } from '@nestjs/swagger';

import {
  ErrorDetailsModel,
  ErrorResponseModel,
} from '../models/errorResponse.model';

export class ErrorBadRequestDto implements ErrorResponseModel {
  @ApiProperty({
    description: 'Main reason why the request is denied',
    type: 'string',
    example: 'Client request error',
  })
  message: string;

  @ApiProperty({
    minimum: 400,
    description: 'Status code request',
    type: 'number',
    example: 400,
  })
  statusCode: number;
  @ApiProperty({
    description: 'Error details',
    type: 'array',
    items: {
      type: 'object',
      properties: {
        key: {
          description:
            'key where is the error. For object in request "key", for array of primitives values "key.0", for array of object values "mainKey.1.childrenKey"',
          type: 'string',
          example: 'rows.0.value',
        },
        target: {
          type: 'object',
          description:
            'Value of key where is the error. Can object or primitives value',
        },
        reason: {
          type: 'array',
          description: 'Reasons cause key is wrong',
          items: {
            type: 'string',
          },
          example: ['field should not be empty'],
        },
      },
    },
  })
  details: ErrorDetailsModel[];
}

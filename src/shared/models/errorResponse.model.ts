export interface ErrorDetailsModel {
  key: string;
  target: unknown;
  reason: string[];
}

export interface ErrorResponseModel {
  statusCode: number;
  message: string;
  details: ErrorDetailsModel[];
}

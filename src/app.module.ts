import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as Joi from 'joi';
import * as moment from 'moment-timezone';

import DatabaseModule from '@shared/database/database.module';

import { BranchesModule } from './branches/branches.module';
import { CampaignsModule } from './campaigns/campaigns.module';
import { CustomersModule } from './customers/customers.module';
import { MerchantsModule } from './merchants/merchants.module';
import { PurchasesModule } from './purchases/purchases.module';
import { RewardsModule } from './rewards/rewards.module';

@Module({
  imports: [
    BranchesModule,
    CampaignsModule,
    CustomersModule,
    MerchantsModule,
    PurchasesModule,
    RewardsModule,
    DatabaseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        host: configService.get('DB_HOST'),
        port: configService.get('DB_PORT'),
        user: configService.get('DB_USER'),
        password: configService.get('DB_PASSWORD'),
        database: configService.get('DB_NAME'),
        timezone: moment.tz('America/Bogota').format('Z'),
      }),
    }),
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        DB_HOST: Joi.string().required(),
        DB_PORT: Joi.number().required(),
        DB_USER: Joi.string().required(),
        DB_PASSWORD: Joi.string().required(),
        DB_NAME: Joi.string().required(),
      }),
    }),
  ],
  providers: [],
})
export class AppModule {}

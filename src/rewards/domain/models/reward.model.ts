export interface RewardModelData {
  id: number;
  points: number;
  customer_id: number;
}

class RewardModel {
  id: number;
  points: number;
  customer_id: number;

  constructor(rewardData: RewardModelData) {
    this.id = rewardData.id;
    this.points = rewardData.points;
    this.customer_id = rewardData.customer_id;
  }
}

export default RewardModel;

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class AssignRewardDto {
  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({
    name: 'points',
    example: 20000,
  })
  readonly points: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    name: 'customer_id',
    example: 1,
  })
  readonly customer_id: number;
}

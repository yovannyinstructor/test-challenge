import { Module } from '@nestjs/common';

import { CampaignsService } from 'campaigns/application/services/campaigns.service';
import { CampaignsRepository } from 'campaigns/infrastructure/repositories/campaigns.repository';
import { RewardsService } from './application/services/rewards.service';
import { RewardsController } from './infrastructure/controllers/rewards.controller';
import { RewardsRepository } from './infrastructure/repositories/rewards.repository';

@Module({
  imports: [],
  controllers: [RewardsController],
  providers: [
    RewardsService,
    RewardsRepository,
    CampaignsService,
    CampaignsRepository,
  ],
})
export class RewardsModule {}

import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiOperation,
  ApiProduces,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import { ErrorBadRequestDto } from '@shared/dtos/error.dto';
import { REWARDS_PATH, REWARDS_TAG } from '@shared/swagger/constants';
import FindOneParams from '@shared/utils/findOneParams';
import PaginationParams from '@shared/utils/paginationParams';

import { RewardsService } from '../../application/services/rewards.service';
import { AssignRewardDto } from '../../domain/dtos/assign-reward.dto';

@ApiTags(REWARDS_TAG)
@Controller(REWARDS_PATH)
@UseInterceptors(ClassSerializerInterceptor)
export class RewardsController {
  constructor(private readonly rewardsService: RewardsService) {}

  @ApiOperation({
    summary: 'Listar todos los puntos obtenidos',
    description: 'Listar todos los puntos obtenidos',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
  })
  @Get()
  getRewards(@Query() { offset, limit, idsToSkip }: PaginationParams) {
    return this.rewardsService.getRewards(offset, limit, idsToSkip);
  }

  @ApiOperation({
    summary: 'Buscar registro de puntos por id',
    description: 'Buscar registro de puntos por id',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
  })
  @Get(':id')
  getRewardById(@Param() { id }: FindOneParams) {
    return this.rewardsService.getRewardById(id);
  }

  @ApiOperation({
    summary: 'Registrar puntos',
    description: 'Registrar puntos',
  })
  @ApiProduces('application/json')
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Success',
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Bad Request',
    type: ErrorBadRequestDto,
  })
  @Post()
  assignReward(@Body() assignRewardDto: AssignRewardDto) {
    return this.rewardsService.assignReward(assignRewardDto);
  }
}

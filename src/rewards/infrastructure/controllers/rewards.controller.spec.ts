import { Test, TestingModule } from '@nestjs/testing';

import { RewardsService } from 'rewards/application/services/rewards.service';
import { RewardsController } from './rewards.controller';

import {
  mockReward,
  mockRewards,
} from '../../../../test/__mocks__/rewards/data/reward.mock';

describe('RewardsController test suite 🧪', () => {
  let controller: RewardsController;
  let service: RewardsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RewardsController],
      providers: [
        {
          provide: RewardsService,
          useValue: {
            getRewards: jest.fn(),
            getRewardById: jest.fn(),
            createReward: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<RewardsController>(RewardsController);
    service = module.get<RewardsService>(RewardsService);
  });

  it('should return an array of purchases ✅', async () => {
    jest.spyOn(service, 'getRewards').mockResolvedValue(mockRewards);

    const result = await controller.getRewards({});

    expect(result).toBe(mockRewards);
  });

  it('should return a reward by id ✅', async () => {
    jest.spyOn(service, 'getRewardById').mockResolvedValue(mockReward);

    const result = await controller.getRewardById({ id: 1 });

    expect(result).toBe(mockReward);
    expect(service.getRewardById).toHaveBeenCalledWith(1);
  });
});

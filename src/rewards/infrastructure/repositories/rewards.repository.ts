import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import DatabaseService from '@shared/database/database.service';
import PostgresErrorCode from '@shared/database/postgresErrorCode.enum';
import { isDatabaseError } from '@shared/types/databaseError';

import { AssignRewardDto } from '../../domain/dtos/assign-reward.dto';
import RewardModel from '../../domain/models/reward.model';

@Injectable()
export class RewardsRepository {
  constructor(private readonly databaseService: DatabaseService) {}

  async get(offset = 0, limit: number | null = null, idsToSkip = 0) {
    const databaseResponse = await this.databaseService.runQuery(
      `
      WITH selected_rewards AS (
        SELECT * FROM rewards
        WHERE id > $3
        ORDER BY id ASC
        OFFSET $1
        LIMIT $2
      ),
      total_rewards_count_response AS (
        SELECT COUNT(*)::int AS total_rewards_count FROM rewards
      )
      SELECT * FROM selected_rewards, total_rewards_count_response
    `,
      [offset, limit, idsToSkip],
    );
    const items = databaseResponse.rows.map(
      (databaseRow) => new RewardModel(databaseRow),
    );
    const count = databaseResponse.rows[0]?.total_rewards_count || 0;
    return {
      items,
      count,
    };
  }

  async getById(id: number) {
    const databaseResponse = await this.databaseService.runQuery(
      `
      SELECT * FROM rewards WHERE id=$1
    `,
      [id],
    );
    const entity = databaseResponse.rows[0];
    if (!entity) {
      throw new NotFoundException();
    }
    return new RewardModel(entity);
  }

  async create(assignRewardData: AssignRewardDto) {
    try {
      const databaseResponse = await this.databaseService.runQuery(
        `
          INSERT INTO rewards (
            points,
            customer_id
          ) VALUES (
            $1,
            $2
          ) RETURNING *
        `,
        [assignRewardData.points, assignRewardData.customer_id],
      );
      return new RewardModel(databaseResponse.rows[0]);
    } catch (error) {
      if (
        !isDatabaseError(error) ||
        !['points', 'customer_id'].includes(error.column)
      ) {
        throw error;
      }
      if (error.code === PostgresErrorCode.NotNullViolation) {
        throw new BadRequestException(
          `A null value can't be set for the ${error.column} column`,
        );
      }
      throw error;
    }
  }
}

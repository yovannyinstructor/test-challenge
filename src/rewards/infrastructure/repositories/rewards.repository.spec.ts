import { Test, TestingModule } from '@nestjs/testing';

import DatabaseService from '@shared/database/database.service';

import { RewardsRepository } from './rewards.repository';

import { mockAssignRewardDto } from '../../../../test/__mocks__/rewards/data/reward.mock';

describe('RewardsRepository test suite 🧪', () => {
  let repository: RewardsRepository;
  let databaseService: DatabaseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RewardsRepository,
        {
          provide: DatabaseService,
          useValue: {
            runQuery: jest.fn(),
          },
        },
      ],
    }).compile();

    repository = module.get<RewardsRepository>(RewardsRepository);
    databaseService = module.get<DatabaseService>(DatabaseService);
  });

  it('should re-throw error if it is not a database error 🛑', async () => {
    const error = new Error('Something went wrong');

    jest.spyOn(databaseService, 'runQuery').mockRejectedValue(error);

    await expect(repository.create(mockAssignRewardDto)).rejects.toThrowError(
      error,
    );
  });
});

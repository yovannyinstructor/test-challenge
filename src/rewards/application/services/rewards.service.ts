import { Injectable, Logger } from '@nestjs/common';
import moment from 'moment-timezone';

import { CampaignModel } from 'campaigns/domain/models/campaign.model';
import { CampaignsRepository } from 'campaigns/infrastructure/repositories/campaigns.repository';
import { CreatePurchaseDto } from 'purchases/domain/dtos/create-purchase.dto';
import { AssignRewardDto } from '../../domain/dtos/assign-reward.dto';
import { RewardsRepository } from '../../infrastructure/repositories/rewards.repository';

@Injectable()
export class RewardsService {
  constructor(
    private readonly rewardsRepository: RewardsRepository,
    private readonly campaignsRepository: CampaignsRepository,
  ) {}

  getRewards(offset?: number, limit?: number, idsToSkip?: number) {
    return this.rewardsRepository.get(offset, limit, idsToSkip);
  }

  getRewardById(id: number) {
    return this.rewardsRepository.getById(id);
  }

  async calculateRewards(createPurchaseDto: CreatePurchaseDto) {
    const { amount, campaign_id, customer_id } = createPurchaseDto;
    const { min_quantity, reward_factor, is_active, start_date } =
      await this.getCampaignById(campaign_id);

    const startDateFormatted = moment(start_date)
      .tz('America/Bogota')
      .format('YYYY-MM-DD')
      .toString();
    const currentDate = moment()
      .tz('America/Bogota')
      .format('YYYY-MM-DD')
      .toString();

    if (
      is_active &&
      amount >= min_quantity &&
      currentDate >= startDateFormatted
    ) {
      Logger.log('assign points');
      const points = (amount / 1000) * reward_factor;
      const rewardData = { points, customer_id };
      this.assignReward(rewardData);
    }
  }

  async getCampaignById(id: number): Promise<CampaignModel> {
    return await this.campaignsRepository.getById(id);
  }

  assignReward(assignRewardDto: AssignRewardDto) {
    return this.rewardsRepository.create(assignRewardDto);
  }
}
